import { ajaxRequest } from "@apis/index"

export const getProductList = () => {
    return ajaxRequest.get('products/');
}

export const getProductDetails = (productId) => {
    return ajaxRequest.get(`products/${productId}`)
}