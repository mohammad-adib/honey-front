import React from 'react';

const Gallery = ({ images, alt }) => {
  let imageSrc;
  if (images.length > 0){
      imageSrc = images[0].file;
  }else{
      imageSrc = '/images/png/empty-product2.png';
  }
  return (
      <div className="gallery__wrapper">
          <div className="gallery__item-wrapper">
            <img className="gallery__image-item" src={imageSrc} alt={alt}/>
          </div>
      </div>
  )
}
 
export default Gallery;