import React from 'react';
import Modal from 'react-modal';

const BasicModal = ({children, isOpen, modalClassName}) => {
  return ( 
    <Modal
      isOpen = {isOpen}
      portalClassName = {`ReactModal ${modalClassName}`}
      overlayClassName = {`${modalClassName}__overlay`}
    >
      {children}
    </Modal>
   );
}
 
export default BasicModal;