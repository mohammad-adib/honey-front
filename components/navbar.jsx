import React from 'react';
import Link from 'next/link';
import {connect} from 'react-redux';
// lib
import {hyperString} from '@lib/string';

const listItems = [
    {
        title: "خانه",
        link: "/"
    },
    {
        title: "محصولات",
        link: "/products/"
    },
    {
        title: "سبد خرید",
        link: "/cart/",
        type: "cart"
    },
    {
        title: "تماس با ما",
        link: "/contact/"
    },
]

const Navbar = (props) => {
    return ( 
        <div className='navbar__container'>
            <ul className="navbar__list-wrapper">
                {listItems.map( (item, index) => (
                    <li key={index} className="navbar__list-item">
                      <Link href={item.link}>
                        <a className="navbar__list-item-link" >
                            {item.title}
                            {(item.type === "cart" && props.cart.length > 0) && 
                                <sup className="navbar__list-item-quantity">
                                  {hyperString(props.cart.length).translateNumbers().toString()}
                                </sup>
                              }
                        </a>
                      </Link>
                    </li>
                  )
                )}
            </ul>
        </div>
    );
}

const mapStateToProps = state => {
  return { cart: state.cart};
};
    

 
export default connect(mapStateToProps)(Navbar);
