import React from 'react';
import Link from 'next/link';
import {hyperString} from "@lib/string";



const ProductItem = ({data}) => {
    
    const hasDiscount = (data.discounted_price < data.base_price);
    const discountPercent = Math.round( (data.base_price - data.discounted_price) / data.base_price * 100 );
    let availability
    let primaryImage, secondaryImage
    if (data.thumbnail){
        primaryImage =  <img className="product__image-primary" src={data.thumbnail} alt={data.english_name}/>;
    }else{
        primaryImage =  <img className="product__image-primary" src="/png/empty-product.png" alt={data.english_name}/>;
    }
    if (data.available){
        availability = {tag: "آماده ارسال", className: "ready"}
    }else{
        availability = {tag: "ناموجود", className: "empty"}
    }
    return ( 
        <Link 
            href="/products/[productId]" 
            as={`/products/${data.id}`}
        >
            <a className="product__container">
                <span className={`product__availability product__availability--${availability.className}`}>
                    {availability.tag}
                </span>
                <div className="product__image-wrapper">
                    {primaryImage}
                </div>
                <div className="product__name">
                    {data.product_name}
                </div>
                <div className="product__info-row product__info-row--spaced product__info-row--price">
                    <div className="product__package-name">
                        {data.package_name}
                    </div>
                    <div className="product__price-container">
                        {hasDiscount &&
                            <div className="product__price-floated">
                                <div className='product__price-crossed'>
                                    {hyperString(data.base_price).formatCurrency().translateNumbers().toString()}
                                </div>                            
                                <div className="product__discount-badge">
                                    {`${hyperString(discountPercent).translateNumbers().toString()}%`}
                                </div>
                            </div>
                        }
                        <div className='product__price'>
                            {hyperString(data.discounted_price).formatCurrency().translateNumbers().toString()}
                            <span className="product__price-label">
                                تومان   
                            </span>
                        </div>
                    </div>
                </div>
            </a>
        </Link>
     );
}
 
export default ProductItem;