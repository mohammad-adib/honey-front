import React from 'react';
// lib
import {hyperString} from '@lib/string';

const QuantitySetter = ({value, incFunc, decFunc, incLimit, decLimit}) => {
  const incLimitPreserved = value < incLimit;
  const incLimitClass = incLimitPreserved ? "quantity-setter__control--disabled" : "";
  const decLimitPreserved = value > decLimit;
  const decLimitClass = decLimitPreserved ? "quantity-setter__control--disabled" : "";
  return ( 
    <div className="quantity-setter">
      <div className="quantity-setter__wrapper">
        <button 
          className={`quantity-setter__control quantity-setter__control--increment ${incLimitClass}`}
          // onClick={incLimitPreserved && incFunc}
          onClick={incLimitPreserved && incFunc}
        >
          +
        </button>
        <div className="quantity-setter__number">
          {hyperString(value).translateNumbers().toString()}
        </div>
        <button
          className={`quantity-setter__control quantity-setter__control--decrement ${decLimitClass}`}
          onClick={decLimitPreserved && decFunc}
        >
          -
        </button>
      </div>
    </div>
  );
}
 
export default QuantitySetter;