import React from 'react';

const TopBanner = () => {
  return ( 
    <div className="top-banner">
      <div className="top-banner__container">
        <img src="/images/png/banner.png" alt="عسل فروش دوره گرد" className="top-banner__image"/>
      </div>
    </div>
  );
}
 
export default TopBanner;