var fontName = 'honey';
var gulp = require('gulp');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var runTimestamp = Math.round(Date.now()/1000);

gulp.task('iconfont', async function(){
  gulp.src(['public/fonts/iconfont/*.svg'])
      .pipe(iconfontCss({
          fontName: fontName, // required
          path: 'public/fonts/iconfont.scss',
          targetPath: '../scss/elements/_element.iconfont.scss',
          fontPath: '../fonts/',
          firstGlyph: 0xF001
      }))
      .pipe(iconfont({
          fontName: fontName,
          formats: ['ttf', 'eot', 'woff', 'woff2'],
          timestamp: runTimestamp,
          fontHeight: 1000,
          normalize: true,
          startUnicode: 0xF001
      }))
      .pipe(gulp.dest('public/fonts/'));
});
