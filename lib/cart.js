import _ from 'lodash';

export const manageProductInCart = (cart, {productData, productQuantity}) => {
  const updatedCart = [...cart];
  const index = _.findIndex(updatedCart,
    (item) => {
      return item.productData.id === productData.id
    } 
  );
  if (index > -1) {
    if (productQuantity > 0){
      updatedCart[index].productQuantity = productQuantity;
    }else{
      updatedCart.splice(index, 1);
    }
  }else{
    updatedCart.push({productData, productQuantity});
  }
  return updatedCart;
}

export const productExistInCart = (cart, productId) => {
  const found = cart.find( item => {
      return item.productData.id === productId
    } 
  );

  return !!(found);

};

export const saveCart = (cart) => {
  localStorage.setItem("cart", JSON.stringify(cart));
};

export const loadCart = () => {
  const cart = JSON.parse(localStorage.getItem("cart"));
  return (cart ? cart : []);
};

export const summerizeCart = cart => {
  const summary = {
    count: 0,
    totalPrice: 0
  };
  summary.count = cart.length;

  if (summary.count > 0){
    const totallPrice = cart.reduce((accum, current) => {
      return accum + current.base_price;
    });
    summary.totalPrice = totallPrice;
  }

  return summary;
}