class HyperString {
  constructor(stringVar){
    if (!stringVar){
      throw Error("Price value cannot be undefined");
    }
    this.value = stringVar;
  };

  formatCurrency = () => {
    const price = this.value;

    const splitPrice = price.split("").reverse();
    let Len3 = Math.floor(price.length / 3);
    if (price.length % 3 ==0) Len3 -= 1;
    for (let i = 0; i < Len3 ;i++){
      splitPrice.splice((i + 1) * 3 + i, 0, ',');
    }

    this.value = splitPrice.reverse().join("");
    return this;
  };

  translateNumbers = () => {
    const price = this.value;

    const splitPrice = price.split("");
    const engMap = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', "%"];
    const farMap = ['۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '۰', "٪"];
    const translatePrice = splitPrice.map(engChar => {
      const farIndex = engMap.findIndex( item => item === engChar);
      if (farIndex === -1) {
        return engChar
      }else{
        return farMap[farIndex];
      }
    })
    this.value = translatePrice.join("");
    return this;
  }

  toString = () => {
    return this.value;
  }
}


export const hyperString = (stringVar) => {
  return new HyperString(stringVar.toString());
}