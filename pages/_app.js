import React from 'react';
import {wrapper, store} from '@redux/store';

const App = ( { Component, pageProps }) => {
  return ( 
      <Component {...pageProps} />
   );
}

 
export default wrapper.withRedux(App);