import React from 'react';
// components
import ContactTemplate from '@templates/contact';

export default function HomeRoute() {
  return (
    <ContactTemplate/>
  )
}
