import React from 'react';
// components
import HomeTemplate from '@templates/home';

export default function HomeRoute() {
  return (
    <HomeTemplate/>
  )
}
