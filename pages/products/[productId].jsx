import React from 'react';
import { getProductDetails } from '@apis/products';
// Layout
import MainLayout from '@layouts/mainLayout';
// Template
import ProductTemplate from '@templates/product';


const ProductRoute = ({ data }) => {
    return ( 
        <MainLayout>
            <ProductTemplate data={data}/>
        </MainLayout>
     );
}

export const getServerSideProps = async ({store, query}) => {
    const res = await getProductDetails(query.productId)
    return {props: {data: res.data}};
}
 
export default ProductRoute;