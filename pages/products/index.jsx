import React from 'react';
// apis
import {getProductList} from '@apis/products';
// templates
import ProductsTemplate from '@templates/Products';

const ProductRoute = ({data}) => {
    return ( 
        <ProductsTemplate data={data}/>
     );
}

export const getServerSideProps = async () => {
    const res = await getProductList();
    return {props: {data: res.data}}
}

export default ProductRoute;
