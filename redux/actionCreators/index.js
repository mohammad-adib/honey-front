import {MANAGE_CART, REMOVE_FROM_CART, LOAD_CART} from '@redux/actionTypes';

// export const manageCart = (productData, productQuantity) => ({
//   type: MANAGE_CART,
//   payload: {productData, productQuantity}
// });


export const manageCart = (productData, productQuantity) => {
  console.log('manageCart Action', MANAGE_CART)
  return {
    type: MANAGE_CART,
    payload: {productData, productQuantity}
  };

};

export const removeFromCart = () => ({
   type: REMOVE_FROM_CART
});

export const loadCart = () => ({
  type: LOAD_CART
});