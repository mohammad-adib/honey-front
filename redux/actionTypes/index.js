export const MANAGE_CART = "MANAGE_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const LOAD_CART = "LOAD_CART";