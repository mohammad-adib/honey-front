import {manageProductInCart} from "@lib/cart";
import {MANAGE_CART, REMOVE_FROM_CART, LOAD_CART} from '@redux/actionTypes';
// lib 
import {saveCart, loadCart} from '@lib/cart';

const cartReducer = (state = {cart: [], loaded: false}, action) => {
  let cart;
  switch (action.type){
    case MANAGE_CART:
      cart = manageProductInCart(state.cart, action.payload);
      saveCart(cart);
      return {...state, "cart": cart};
    // case REMOVE_FROM_CART:
    //     return {...state, counter: (state.counter-1)} ;
    case LOAD_CART:
      cart = loadCart();
      return {...state, "cart": cart, loaded: true};
    default:
        return state;
  }
}

export default cartReducer;