import {createStore} from 'redux';
import {createWrapper} from 'next-redux-wrapper';
import cartReducer from '@redux/reducers/cartReducer';

export const store = () => createStore(cartReducer);

export const wrapper = createWrapper(store, {debug: true});

