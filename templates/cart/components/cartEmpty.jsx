import React from 'react';
import Link from 'next/link';


const CartEmpty = () => {
  return ( 
    <div className="cart__empty-wrapper">
      <div className="cart__empty-img">

      </div>
      <div className="cart__empty-text">
        سبد خرید شما خالی است!
      </div>
      <div className="cart__empty-link-wrapper">
        <Link href="/products/">
            <a className="cart__empty-link-text" >
                لیست محصولات    
            </a>
          </Link>
        </div>
    </div>
  );
}
 
export default CartEmpty;