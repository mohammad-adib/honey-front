import React from 'react';
import {connect} from 'react-redux';
// components
import QuantitySetter from '@components/quantitySetter';
// redux
import {manageCart} from '@redux/actionCreators';

const incAction = (action ,data, quantity) => {
  action(data, quantity + 1);
}

const decAction = (action, data, quantity) => {
  action(data, quantity - 1);
}

const remAction = (action, data) => {
  action(data, 0);
}

const CartItem = ({manageCart ,data, quantity}) => {
  const productName = `${data.product_name} - ${data.package_name}`;
  return ( 
    <div className="cart__item">
        <div className="cart__item-image-wrapper">
          <img src={data.thumbnail} alt={productName}/>
        </div>
        <div className="cart__item-info-wrapper">
          <div className="cart__item-name">
            {productName}
          </div>
          <div className="cart__item-control-block">
            <QuantitySetter 
              value={quantity} 
              incFunc={() => {incAction(manageCart, data, quantity)}}
              incLimit={10} 
              decFunc={() => {decAction(manageCart, data, quantity)}} 
              decLimit={1}
            />
          </div>
          <div className="cart__item-remove-container">
            <button className="cart__item-remove-action"
              onClick={() => {remAction(manageCart, data)}}
            >
            حذف 
            </button>
          </div>
        </div>
    </div>
  );
}


const mapDispatchToProps = {
  manageCart
};
 
export default connect(null, mapDispatchToProps)(CartItem);