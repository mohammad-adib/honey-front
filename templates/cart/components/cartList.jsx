import React from 'react';
// components
import CartItem from './cartItem';

const CartList = ({cart}) => {
  return ( 
    <ul className="cart__list">
      {cart.map( (item, index) => {
        return (
          <li key={index} className="cart__item-wrapper">
            <CartItem data={item.productData} quantity={item.productQuantity} />
          </li>
        );
      })}
    </ul>
  );
}
 
export default CartList;
