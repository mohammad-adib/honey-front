import React from 'react';
// lib
import {summerizeCart} from '@lib/cart';

const OrderPlacement = ({cart}) => {
  const {count, totalPrice} = summerizeCart(cart);
  console.log(totalPrice);
  if (count == 0) return <div></div>;

  return ( 
    <div className="cart__order-wrapper">
      <div className="cart__order-info-row">
        <div className="cart__order-info-block">
          <div className="cart__order-info-label">
            قیمت کالاها
          </div>
          <div className="cart__order-info-value">
            {/* {totallPrice} */}
          </div>
        </div>
      </div>
    </div>
  );
}
 
export default OrderPlacement;