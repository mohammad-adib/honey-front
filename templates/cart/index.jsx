import React from 'react';
import {connect} from 'react-redux';
// components
import CartList from './components/cartList';
import CartEmpty from './components/cartEmpty';
import OrderPlacement from './components/orderPlacement';
// layouts
import MainLayout from '@layouts/mainLayout';

const CartTemplate = ({cart}) => {
  
  const cartBody = (cartLength) => {
    if (cartLength > 0){
      return <CartList cart={cart}/>
    }else{
      return <CartEmpty />
    }
  }

  return ( 
    <MainLayout>
      <div className="cart">
        <header className="cart__header-wrapper">
          <div className="cart__header">
            سبد خرید
          </div>
        </header>
        <section className="cart__main">
          <div className="cart__body">
            {cartBody(cart.length)}
          </div>
          <aside className="cart__order-pane">
            <OrderPlacement cart={cart}/>
          </aside>
        </section>
      </div>
    </MainLayout>
  );
}

const mapStateToProps = state => {
  return { cart: state.cart};
};


export default connect(mapStateToProps, null)(CartTemplate);
