import React from 'react';
// layouts
import MainLayout from '@layouts/mainLayout';

const HomeTemplate = (props) => {
  return ( 
    <MainLayout>
      <div className="Home">
        Home
      </div>
    </MainLayout>
  );
}
 
export default HomeTemplate;