import React from 'react';
import {useEffect} from 'react';
import {connect} from 'react-redux';
// redux
import {loadCart} from '@redux/actionCreators';

const DataProvider = ({children, loaded, loadCart}) => {
  useEffect(() => {
    if (!loaded){
      loadCart();
    }
  })
  return ( 
    <div className="data-provider">
      {children}
    </div>
  );
}

const mapStateToProps = state => {
  return { loaded: state.loaded};
};

const mapDispatchToProps = {
  loadCart
};

export default connect(mapStateToProps, mapDispatchToProps)(DataProvider);