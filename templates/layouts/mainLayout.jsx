import React from 'react';
// components
import Nabvar from "@components/navbar";
import Footer from "@components/footer";
import TopBanner from "@components/topBanner";
import Head from 'next/head';
import DataProvider from "@layouts/dataProvider";

const MainLayout = (props) => {
  return ( 
    <DataProvider>
      <Head>
        <title>عسل فروش دوره گرد</title>
        <link rel="stylesheet" media="screen and (min-device-width: 750px)" href="/css/app.desktop.css" />
        <link rel="stylesheet" media="screen and (max-device-width: 750px)" href="/css/app.mobile.css" />
      </Head>
      <TopBanner/>
      <Nabvar/>
      <div className="main-content">
        {props.children}
      </div>
      <Footer/>
    </DataProvider>
   );
}
 
export default MainLayout;