import React from 'react';

const InfoBlock = ({data}) => {
  return (
    <div className="product-details__info-column">
        <div className="product-details__name">
            {`${data.product_name} - ${data.package_name}`}
        </div>
        <section className="prodct-details__health">
            <header className="product-details__health-header">
                خواص درمانی
            </header>
            <div className="product-details__health-list">
                {`${data.health_benefits}`}
            </div>
            <div className="product-details__description">
                <div className="product-details__description-header">
                    توضیحات
                </div>
                <div className="product-details__description-text">
                    {`${data.description}`}
                </div>
            </div>
        </section>
    </div>
);
}
 
export default InfoBlock;