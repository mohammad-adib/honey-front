import React, {useState} from 'react';
import {connect} from 'react-redux';
import { useRouter } from 'next/router';
// lib
import {hyperString} from "@lib/string";
import {productExistInCart} from "@lib/cart";
// redux
import {manageCart} from '@redux/actionCreators';



const PurchaseBlock = ({data, manageCart, cart}) => {
  const hasDiscount = (data.discounted_price < data.base_price);
  const discountPercent = Math.round( (data.base_price - data.discounted_price) / data.base_price * 100 );
  const { query } = useRouter();
  const [purchaseModalOpen, setPurchaseModalOpen] = useState(false);

  const openPurchaseModal = () => {
    setPurchaseModalOpen(true);
  }

  const handlePurchase = (purchaseAction, productData, productQuantity) => {
    purchaseAction(productData, productQuantity);
    setPurchaseModalOpen(true);
  }

  let elementPurchaseControl;

  if (productExistInCart(cart, query.productId)){

  }

  return (
    <div className="product-details__purchase-container">
      <div className="product-details__purchase-row">
        <div className="product-details__warranty">
          {data.health_tests}
        </div>
      </div>
      <div className="product-details__purchase-row">
        <div className="product-details__shipping">
          آماده‌ ارسال به مشتری
        </div>
      </div>
      <div className="product-details__purchase-row">
        <div className="product-details__price-container">
          <div className="product-details__price-title">
              قیمت برای مصرف کننده:
          </div>
          <div className="product-details__price-wrapper">
            {hasDiscount &&
                <div className="product-details__discount-wrapper">
                    <div className="product-details__price-crossout">
                        {hyperString(data.base_price).translateNumbers().formatCurrency().toString()}
                    </div>
                    <div className="product-details__discount-badge">
                        {hyperString(discountPercent.toString()).translateNumbers().toString()}
                        %
                    </div>
                </div>
            }
            <div className="product-details__price-value">
                {hyperString(data.discounted_price).translateNumbers().formatCurrency().toString()}
                <span className="product-details__price-label">
                    تومان   
                </span>
            </div>
          </div>
          <div className="product-details__purchase-button-container">
            <button 
              className="product-details__purchase-button"
              onClick={() => handlePurchase(manageCart, data, 1)}
              >
                افزودن به سبد خرید   
            </button> 
          </div>
        </div>
      </div>
          {/* <div className="purchase-modal__container">
            کالا به سبد خرید اضافه شد
          </div> */}
    </div>
  );
}

const mapStateToProps = state => {
  return { cart: state.cart};
};

const mapDispatchToProps = {
  manageCart
};

 

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseBlock);