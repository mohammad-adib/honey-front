import React from 'react';
import {hyperString} from "@lib/string";
// common components
import Gallery from '@components/gallery';
// specfic components
import InfoBlock from './components/infoBlock';
import PurchaseBlock from './components/purchaseBlock';

const ProductTemplate = ({ data }) => {
    const { images, product_name, package_name } = data.Item;
    return ( 
        <div className="product-details">
            <article className="product-details__container">
                <section className="product-details__gallery">
                    <Gallery images={images} alt = {`${package_name}, ${product_name}`} />  
                </section>
                <section className="product-details__info-container">
                    <InfoBlock data={data.Item}/>
                </section>
                <section className="product-details__pruchase-panel">
                    <PurchaseBlock data={data.Item} />
                </section>
            </article>
        </div>
     );
}

 
export default ProductTemplate;