import React from 'react';
// layouts
import MainLayout from '@layouts/mainLayout';
// components
import ProductItem from '@components/productItem';

const ProductsTemplate = ( {data} ) => {
    return ( 
      <MainLayout>
        <div className="product-list">
            <h1 className="product-list__header">
                لیست محصولات
            </h1>
            <div className="product-list__grid">
                {data.ListItems.map(productData => (
                    <ProductItem key={productData.id} data={productData}/>
                ))}
            </div>
        </div>
      </MainLayout>
     );
}



export default ProductsTemplate;
